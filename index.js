///1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

///Екранування - моливість перетворити сцеціальний символ на звичайний. Це робиться за допомогою
///символу "\", чкий розташовується перед спецсимволом, який треба екранувати.

///2.Які засоби оголошення функцій ви знаєте?
/// Є два способи оголошення функцій.
/// Перший: function declaration.
/// function one() {
///     return "one";
/// }
/// Другий: function expression.
/// let two = function () {
///     return "two";
/// };

///3. Що таке hoisting, як він працює для змінних та функцій?

///Hoisting для функцій дає можливість викликати функцію до оголошення (у випадку function declaration).
/// Якщо змінну оголошувати за допомогою let, const, потрібно оголошувати до ініціалізації. І вже
/// після цього використовувати. Якщо використовувати до - отримаємо помилку.
/// А от у випадку оголошення змінної за допомогою var - можливо спочатку присвоїти значення, а
/// потім оголосити змінну.


function createNewUser() {
  const firstName = prompt("Введіть Ваше ім'я");
  while (firstName === null || firstName === "" || !isNaN(firstName)) {
    firstName = prompt("Введіть Ваше ім'я", firstName);
  }

  const lastName = prompt("Введіть Ваше прізвище");
  while (lastName === null || lastName === "" || !isNaN(lastName)) {
    lastName = prompt("Введіть Ваше прізвище", lastName);
  }

  const birthday = prompt("Введіть Вашу дату народження в форматі дд.мм.гггг");
  while (birthday === null || birthday === "" || birthday.length < 10) {
    birthday = prompt("Введіть Вашу дату народження в форматі дд.мм.гггг");
  }

  newUser = {
    firstName: firstName,
    lastName: lastName,
  };
  function getLogin() {
    return newUser.firstName[0].toUpperCase() + newUser.lastName.toLowerCase();
  }
  console.log(getLogin());
  function getAge() {
    let day = parseInt(birthday.substring(0, 2));
    let month = parseInt(birthday.substring(3, 5));
    let year = parseInt(birthday.substring(6, 10));

    let today = new Date();
    let birthDate = new Date(year, month - 1, day);
    let age = today.getFullYear() - birthDate.getFullYear();
    let i = today.getMonth() - birthDate.getMonth();
    if (i < 0 || (i === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }
  console.log(getAge());
  function getPassword() {
    let year = parseInt(birthday.substring(6, 10));
    return `${getLogin()}${year}`;
  }
  console.log(getPassword());
}
createNewUser();

